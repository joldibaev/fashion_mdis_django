from django.shortcuts import render
from tensorflow import keras
from tensorflow.keras.datasets import fashion_mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras import utils
import numpy as np
import matplotlib.pyplot as plt

from pathlib import Path

from fashion.utils import get_base64


def index(request):
    (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

    im = []
    for i in range(0, 150):
        im.append(get_base64(x_train[i]))

    return render(request, 'index.html', context={
        'images': im
    })


def predict(request, id):
    BASE_DIR = Path(__file__).resolve().parent.parent

    # В Keras встроены средства работы с популярными наборами данных
    (x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

    classes = ['футболка', 'брюки', 'свитер', 'платье', 'пальто', 'туфли', 'рубашка', 'кроссовки', 'сумка', 'ботинки']

    x_train = x_train.reshape(60000, 784)

    # Векторизованные операции
    # Применяются к каждому элементу массива отдельно
    x_train = x_train / 255

    y_train = utils.to_categorical(y_train, 10)

    new_model = None
    try:
        new_model = keras.models.load_model(BASE_DIR / 'model.h5')
    except Exception as e:
        print(e)

    if not new_model:
        print('Creating new model')
        # Создаем последовательную модель
        model = Sequential()
        # Входной полносвязный слой, 800 нейронов, 784 входа в каждый нейрон
        model.add(Dense(800, input_dim=784, activation="relu"))
        # Выходной полносвязный слой, 10 нейронов (по количеству рукописных цифр)
        model.add(Dense(10, activation="softmax"))

        model.compile(loss="categorical_crossentropy", optimizer="SGD", metrics=["accuracy"])

        print(model.summary())

        print('Training model')
        model.fit(x_train, y_train,
                  batch_size=200,
                  epochs=100,
                  verbose=1)

        print('Saving model')
        model.save(BASE_DIR / 'model.h5')
    else:
        print('Using already trained model')
        model = new_model
    predictions = model.predict(x_train)

    n = id
    np.argmax(predictions[n])
    np.argmax(y_train[n])
    image = get_base64(x_train[n].reshape(28, 28))
    result = classes[np.argmax(y_train[n])]
    print(result)
    return render(request, 'predict.html', context={'image': image, 'result': result})

import io
from base64 import b64encode

import numpy as np
from PIL import Image
from matplotlib import cm


def get_base64(nparray2d):
    image = Image.fromarray(np.uint8(cm.gist_earth(nparray2d) * 255))

    in_mem_file = io.BytesIO()
    image.save(in_mem_file, format="PNG")

    in_mem_file.seek(0)
    img_bytes = in_mem_file.read()

    base64_encoded_result_bytes = b64encode(img_bytes)
    base64_encoded_result_str = base64_encoded_result_bytes.decode('ascii')

    return base64_encoded_result_str
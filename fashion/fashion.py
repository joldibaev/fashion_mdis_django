from PIL import Image
from matplotlib import cm
from tensorflow import keras
from tensorflow.keras.datasets import fashion_mnist
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras import utils
import numpy as np
import matplotlib.pyplot as plt

from pathlib import Path

BASE_DIR = Path(__file__).resolve().parent.parent

# В Keras встроены средства работы с популярными наборами данных
(x_train, y_train), (x_test, y_test) = fashion_mnist.load_data()

classes = ['футболка', 'брюки', 'свитер', 'платье', 'пальто', 'туфли', 'рубашка', 'кроссовки', 'сумка', 'ботинки']

plt.figure(figsize=(10, 10))
for i in range(100, 150):
    plt.subplot(5, 10, i - 100 + 1)
    plt.xticks([])
    plt.yticks([])
    plt.grid(False)
    plt.imshow(x_train[i], cmap=plt.cm.binary)
    plt.xlabel(classes[y_train[i]])

x_train = x_train.reshape(60000, 784)

# Векторизованные операции
# Применяются к каждому элементу массива отдельно
x_train = x_train / 255

print(y_train[0])

y_train = utils.to_categorical(y_train, 10)

print(y_train[0])

new_model = None
try:
    new_model = keras.models.load_model(BASE_DIR / 'model.h5')
except Exception as e:
    print(e)

if not new_model:
    print('Creating new model')
    # Создаем последовательную модель
    model = Sequential()
    # Входной полносвязный слой, 800 нейронов, 784 входа в каждый нейрон
    model.add(Dense(800, input_dim=784, activation="relu"))
    # Выходной полносвязный слой, 10 нейронов (по количеству рукописных цифр)
    model.add(Dense(10, activation="softmax"))

    model.compile(loss="categorical_crossentropy", optimizer="SGD", metrics=["accuracy"])

    print(model.summary())

    print('Training model')
    model.fit(x_train, y_train,
              batch_size=200,
              epochs=10,
              verbose=1)

    print('Saving model')
    model.save(BASE_DIR / 'model.h5')
else:
    print('Using already trained model')
    model = new_model
predictions = model.predict(x_train)

# Меняйте значение n чтобы просмотреть результаты распознавания других изображений
n = 0
plt.imshow(x_train[n].reshape(28, 28), cmap=plt.cm.binary)
plt.show()

print(predictions[n])

np.argmax(predictions[n])

print(classes[np.argmax(predictions[n])])

np.argmax(y_train[n])

print(classes[np.argmax(y_train[n])])
